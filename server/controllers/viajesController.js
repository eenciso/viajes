// Importando el modelo Viaje
const Viaje = require('../models/Viajes');

// Exportar multiples porciones de Codigo a diferencia de exportdefault que exporta todo el archivo
exports.mostrarViajes = async (req, res) => {
	const viajes = await Viaje.findAll()
	res.render('viajes', {
		pagina : 'Próximos viajes',
		viajes
	});
}

exports.mostrarViaje = async (req, res) => {
	const viaje = await Viaje.findByPk(req.params.id)
	res.render('viaje', {
		viaje
	});
}