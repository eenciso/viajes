// Importando Sequelize ORM
const Sequelize = require('sequelize');
// Importando archivo Config Base de Datos
const sequelizeDB = require('../config/database');

// Definiendo el modelo
const Viaje = sequelizeDB.define('viaje', {
	titulo: { type: Sequelize.STRING },
	precio: { type: Sequelize.STRING },
	fecha_ida: { type: Sequelize.DATE },
	fecha_vuelta: { type: Sequelize.DATE },
	imagen: { type: Sequelize.STRING },
	descripcion: { type: Sequelize.STRING },
	disponibles: { type: Sequelize.STRING }
});
module.exports = Viaje;

// Probando Conexion con la base de datos
// sequelizeDB.authenticate()
// 	.then(() => {
// 		console.log('Connection has been established successfully.');
// 	})
// 	.catch(err => {
// 		console.error('Unable to connect to the database:', err);
// 	});