// Importando el modelo Testimonial
const Testimonial = require('../models/Testimoniales');


// Exportar multiples porciones de Codigo a diferencia de exportdefault que exporta todo el archivo
exports.mostrarTestimoniales = async (req, res) => {
	const testimoniales = await Testimonial.findAll()
	res.render('testimoniales', {
		pagina : 'Testimoniales',
		testimoniales
	})
}

exports.agregarTestimonial = async (req, res) => {
		// Validar campos, req.body lee los campos enviados en el form
		let {nombre, correo, mensaje} = req.body;
		let errores = [];
		if (!nombre) {
			errores.push({'mensaje': 'Agrega tu nombre'})
		}
		if (!correo) {
			errores.push({'mensaje': 'Agrega tu correo'})
		}
		if (!mensaje) {
			errores.push({'mensaje': 'Agrega tu mensaje'})
		}

		// Revisar posibles errores
		if (errores.length > 0) {
			const testimoniales = await Testimonial.findAll()
			// Muestra la vista con errores, se extraen las variables del arreglo de errores
			res.render('testimoniales', {
				pagina : 'Testimoniales',
				testimoniales,
				errores,
				nombre,
				correo,
				mensaje
			})
		} else {
			// Lo Almacena en la Base de Datos
			await Testimonial.create({
				nombre,
				correo,
				mensaje
			})
			res.redirect('/testimoniales')
		}
	}