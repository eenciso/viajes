const express = require('express');
const router = express.Router();

// Controladores
const nosotrosController = require('../controllers/nosotrosController.js');
const homeController = require('../controllers/homeController.js');
const viajesController = require('../controllers/viajesController.js');
const testimonialesController = require('../controllers/testimonialesController.js');

module.exports = function () {
	// Index
	router.get('/', homeController.consultasHomePage);
	// Nosotros
	router.get('/nosotros', nosotrosController.infoNosotros);
	// Viajes
	router.get('/viajes', viajesController.mostrarViajes);
	// Viaje Individual
	router.get('/viajes/:id', viajesController.mostrarViaje);
	// Testimoniales
	router.get('/testimoniales', testimonialesController.mostrarTestimoniales);
	// Formulario de Testimoniales
	router.post('/testimoniales', testimonialesController.agregarTestimonial);

	return router;
}