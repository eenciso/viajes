// Importando Express
const express = require('express');
// Importando Path
const path = require('path');
// Importando Body Parser
const bodyParser = require('body-parser');
// Importando Rutas
const routes = require('./routes');
// Importando archivo Config
const configs = require('./config');
// dotenv - Se emplea para configurar variables de entorno
require('dotenv').config({ path: '.env.local' });

// Configurando Express
const app = express();

// Habilitando PugJs
app.set('view engine', 'pug');

// Añadiendo Vistas
app.set('views', path.join(__dirname, './views'));

// Cargando carpeta estatica 'public'
app.use(express.static('public'));

// Validar si se esta en ambiente de desarrollo o produccion
const config = configs[app.get('env')];
// Se crea la variable para el sitio web
app.locals.titulo = config.nombreSitio;

// Muestra el año Actual y obtiene la Ruta Actual de la App
app.use((req, res, next) => {
	// Creando Fecha
	const fecha = new Date();
	// La fecha se pasa mediante locals
	res.locals.fechaActual = fecha.getFullYear();
	// Obteniendo la ruta Actual de la App despues de la '/'
	res.locals.ruta = req.path;
	return next();
});

// Se ejecuta el Body-Parser
app.use(bodyParser.urlencoded({extended: true}));

// Carga de Rutas
app.use('/', routes());

// PUERTO y HOST para la App
const host = process.env.HOST || '0.0.0.0';
const port = process.env.PORT || 3000;

// Puerto
app.listen(port, host, () => console.log('El servidor esta Funcionando'));