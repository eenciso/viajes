// Importando Sequelize ORM
const Sequelize = require('sequelize');
// Importando archivo Config Base de Datos
const sequelizeDB = require('../config/database');

const Testimonial = sequelizeDB.define('testimoniales', {
	nombre: { type: Sequelize.STRING },
	correo: { type: Sequelize.STRING },
	mensaje: { type: Sequelize.STRING }
});

module.exports = Testimonial;