// Importando el modelo Viaje
const Viaje = require('../models/Viajes');
// Importando el modelo Testimonial
const Testimonial = require('../models/Testimoniales');

// Exportar multiples porciones de Codigo a diferencia de exportdefault que exporta todo el archivo
exports.consultasHomePage = async (req, res) => {

	const viajes = await Viaje.findAll({ limit: 3 });
	const testimoniales = await Testimonial.findAll({ limit: 3 });

	res.render('index', {
		pagina : 'Próximos viajes',
			// Pasa la clase home solo cuando se accede a la ruta / y se reenderiza con la misma
			clase: 'home',
			viajes,
			testimoniales
		})
}